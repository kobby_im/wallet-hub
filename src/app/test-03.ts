/**
 * Update the following components to meet the requirements : 
 * 
 * * Bind [email] property to input[name="email"]
 * * Bind [password] property to input[name="password"]
 * 
 * Without using angular forms, validate both fields so that :
 * * email is in correct format ( ex: ends with @a.com)
 * * password contains at least one special character, one upper case character, one lower case character, one number and a minium of 8 characters in length
 * * The fields should be validated when trying to submit the form
 * * Prevent the form from doing an actual form submit and instead, after validation pass, turn on the [logged_in] flag
 * 
 * You can add error messages below each field that shows if the field is not valid
 */
import { Component, NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { CommonModule } from '@angular/common';

@Component({
    selector: 'ng-app',
    template: `<form>
                    <h2>Login</h2>
                    <br/>
                    <input (input)="changeFn($event)"  type="email" [value]="email" name="email" />
                    <p><small style="color:red;">{{results.email.msg}}</small></p> 
                    <br/>
                    <input (input)="changeFn($event)"  type="password" [value]="password" name="password" />
                    <p><small tyle="color:red;">{{results.password.msg}}</small></p> 
                    <button (click)="submit($event)" type="submit">Submit</button>
                    <br/><br/>
                    <div *ngIf="logged_in">Logged In!</div>
                </form>`
})
export class Test03Component {
    results = {
        email: {
            msg: "",
            validate: function (email: string) {
                const isValid = email.match(
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                );
                return { isValid, msg: isValid ? "" : "This is not a valid email" }
            }
        },
        password: {
            msg: "",
            validate: function (password: string) {
                let msg
                let isValid = false
                if (!password) {
                    msg = "please fill password"
                    return { msg, isValid }
                }
                if (password.length < 8) {
                    msg = "Password length must be atleast 8 letter"
                    return { msg, isValid }
                }
                if (!password.match(/(?=.*\W)/)) {

                    msg = "Password must be contain at least one special letter"
                    return { msg, isValid }
                }
                if (!password.match(/(?=.*[a-z])/)) {

                    msg = "Password must contain at least one lowercase letter"
                    return { msg, isValid }
                }
                if (!password.match(/(?=.*[A-Z])/)) {

                    msg = "Password must contain at least one uppercase letter"
                    return { msg, isValid }
                }
                if (!password.match(/(?=.*\d)/)) {

                    msg = "Password must contain at least one digit"
                    return { msg, isValid }
                }



                return { isValid: true, msg }
            }
        }
    }

    changeFn(event) {
        this[event.target.name] = event.target.value

    }

    submit(e) {
        e.preventDefault()
        let isValid = true;
        ["email", "password"].forEach(key => {
            const options = this.results[key];
            const result = options.validate(this[key])
            options.msg = result.msg
            isValid = isValid && result.isValid
        })

        if (isValid) {
            this.logged_in = true
        }

    }

    email: string = "";
    password: string = "";

    logged_in = false;
}

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: "",
                component: Test03Component
            }
        ])
    ],
    declarations: [Test03Component]
})
export class Test03Module { };