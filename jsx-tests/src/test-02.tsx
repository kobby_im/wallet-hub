/**
 * In the following React template, modify the component so that the counter correctly displays and it increments by one whenever the button is pressed.
 * You are free to add classes and styles, but make sure you leave the element ID's as they are.
 */

import React, { useState } from "react";
import ReactDOM from "react-dom";

type State = {
  counter: number;
};

class Counter extends React.Component {
  constructor(props) {
    super(props);
  }
  state: State = { counter: 0 };

  handleIncrease = () => {
    this.setState((prevState: State) => {
      return { ...prevState, counter: prevState.counter + 1 };
    });
  };

  render() {
    return (
      <div id="mainArea">
        <p>
          button count: <span>{this.state.counter}</span>
        </p>
        <button id="mainButton" onClick={this.handleIncrease}>
          Increase
        </button>
      </div>
    );
  }
}

ReactDOM.render(<Counter />, document.getElementById("test-02"));
